
// All data types in dart are Objects.
// Therefore , their initial value is by default null

// Variable declaration
void main(){


  // Numbers: int
  int score = 23;
  print(score);

  // Numbers: double
  double percentage = 93.4;
  print(percentage);

  // String
  String name3 = "Henry";
  var company = "Google";
  print(name3);
  print(company);

  // Boolean
  bool isValid = true;
  var isAlive = false;
  print(isValid);
  print(isAlive);



  int age = 10;
  // or
  var age1 = 15;
  print(age);
  print(age1);


  String name = "Jarkko";
  var name1 = "Jarkko";
  print(name);
  print(name1);
}