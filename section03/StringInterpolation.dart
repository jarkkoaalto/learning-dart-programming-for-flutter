

void main() {
  // Litearals
  2;
  "John";
  5.4;

  // Various ways to define String Literals in Dart
  String s1 = 'Single';
  String s2 = "Double";
  String s3 = 'It\'s easy';
  String s4 = "It's easy";

  String s5 = "This is going to be a very long string. "
      "This is just a sample String demo in Dart Programming Language";


  // String Interpolation
  String name = "Kevin";
  String message = "My name is $name";
  print(message);
  print("The number of characters in String kevin is ${name.length}");


  int l = 20;
  int b = 10;

  print("The sum of l ${l} and b ${b} = ${l+b}");
  print("The are of rectangle is ${l*b}");
}