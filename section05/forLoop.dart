

void main(){
    // For Loop
    for(int i = 0; i<= 10;i++){
      print(i);
    }

    // Write a program to find the even numbers between 1 to 10
    for(int j = 0; j<=10;j++){
      if(j%2 == 0){
        print(j);
      }
    }

    // for .. in loop
    List planetList= ["Mercury","Venus","Mars","Earth"];
      for(String planet in planetList){
         print(planet);
      }
}