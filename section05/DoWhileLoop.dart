

void main(){

  // do while loop
  // Write a program to find the even numbers between 1 to 10

  int i =0;
  do {
      if(i % 2 == 0) {
          print(i);
      }
      i++;
  }while(i<=10);

}