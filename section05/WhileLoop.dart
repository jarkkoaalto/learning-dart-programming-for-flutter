
void main(){

  // While loop
  // Write a program to find the even number between 1 to 10

  var i = 0;
  while(i<=10){
    if(i % 2 == 0) {
      print(i);
    }
    i++;
  }
}