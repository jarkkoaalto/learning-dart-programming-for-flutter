void main(){
  // For Loop
  for(var i = 0; i<4; i++){
    print("$i Hello from For loop");
  }

  // While loop
  int i = 0;
  while(i<4){

    print("$i hello from While loop");
    i=i+1;
  }

  //do while loop

  var u = 0;
  do{
    print("$u hello from do While loop");
    u++;
  }while(u <=5);
}