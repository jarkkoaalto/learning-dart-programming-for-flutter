# learning_dart_programming_for_flutter

A web app that uses [AngularDart](https://webdev.dartlang.org/angular) and
[AngularDart Components](https://webdev.dartlang.org/components).

Created from templates made available by Stagehand under a BSD-style
[license](https://github.com/dart-lang/stagehand/blob/master/LICENSE).

Learn Dart Programming from scratch by Google. Prepare yourself for Flutter apps for Android and iOS and developing
 Web apps using Dart programming language.

Dart is an object-oriented language which can optionally trans compile into JavaScript. 
It supports a varied range of programming aids like interfaces, classes, collections, generics, and optional typing.

Dart can be extensively used to create single-page web apps, Android and iOS apps. 
Single-page applications enable navigation between different screens of the website without 
loading a different webpage in the browser.

Course content:

###### Section: 2 Getting Started
###### Section: 3 Exploring Variables and Data Types
###### Section: 4 Control Flow Statements
###### Section: 5 Loop Control Statements
###### Section: 6 Exploring Functions
###### Section: 7 Exception Handling
###### Section: 8 Object Oriented Dart Programming
###### Section: 9 Leveraging Object Oriented Dart Programming Concepts
###### Section: 10 Lambdas and Higher-Order Functions
###### Section: 11 Dart Collection Framework