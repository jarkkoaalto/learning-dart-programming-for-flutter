
  // OBjectives
  /*
    1. define a function
    2. pass parameters to a function
    3. return value from a function
    4. test that by default a function return null
   */

  void main(){
  findPerimeter((4), 8);
  int recarea = getArea(3,4);
  print("The Area is $recarea");
  }

  void findPerimeter(int length, int breadth){
      int perimeter = 2 * (length + breadth);
      print("The perimeter is $perimeter");
  }

  int getArea(int length, int breadth){
    int area = length * breadth;
    return area;
  }