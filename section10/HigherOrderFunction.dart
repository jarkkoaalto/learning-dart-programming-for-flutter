
/*
  Higher-Order function
      can accept function as a parameter
      can return a function
 */

void main(){
      // Example one: passing function to higher-order function
  Function addn = (a,b) => print(a+b);
  SomeFunction("Hi there", addn);

  // Example 2. Receiving Function from higher-order function
    var myF = taskToPerform();
    print(myF(100));
}

// Example 1 accepts function as parameter
void SomeFunction(String message, Function myFunc){ // Higher-order function
  print(message);
  myFunc(56,76);
}

// Example two: returns a function
Function taskToPerform(){
    Function multiplyFour = (int number) => number * 5;
    return multiplyFour;
}