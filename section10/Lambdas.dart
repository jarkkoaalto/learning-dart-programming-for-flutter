/*
  Lambda function without name
  also knoen as anonumous function or lambda
 */

void main(){

  // 1.st way
  Function a = (int a, int b){
            var sum = a+b;
            print(sum);
        };

  var b = (int number){
    return  number * 4;
  };

  // 2nd way
  Function a2 = (int a, int b) => print(a+b);
  var b2 =(int number) => number * 6;



  // Calling lambda function
  a(4, 5);
  a2(12,34);
  print(b(5));
  print(b2(6));
}

 // Normal function
void addMyNumbers(int a, int b){
  var sum = a+b;
  print(sum);
}