/*
  Inheritence demo
  Exploring method overloading
  dart super.eat() -> @Override

 */

void main() {
  var dog = Dog();
  dog.bread = "Labrador";
  dog.color = "Black";
  dog.bark();
  dog.eat();
  print("Dog color " + dog.color);

  var cat = Cat();
  cat.color = "White";
  cat.meow();
  cat.age = 3;

  var animal = Animal();
  animal.eat();
  animal.color = "Brown";
}

class Animal{
  String color;

  void eat(){
    print("Animal is Eating !");
  }
}

class Dog extends Animal{
  String bread;

  void bark(){
    print("Barking");
  }

  void eat() {
    print("Dog is eating !");
    super.eat();
    print("We need Pizza also !!");
  }
}

class Cat extends Animal{
  int age;

  void meow(){
    print("Meow");
  }
}