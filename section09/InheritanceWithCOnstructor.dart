/*
    Inheritance with default constructor and parameterized constructor
    Inheritance with named construcror
 */

void main(){

  var obj1 = Dog("labrador", "Bloack");
  print("");
  var obj2 = Dog("Husky","Black and White");
  print("");
  var obj3 = Dog.myNamedConstructor("Pug","Brown");

}

class Animal{
  String color;

  Animal(String color){
    this.color = color;
    print("Animal class constructor");
  }
  Animal.myAnimalNamedConstructor(){
    print("Animal class named constructor");
  }
}

class Dog extends Animal{
  String breed;
  Dog(String breed, String color) :  super.myAnimalNamedConstructor(){
    this.breed = breed;
    print("Dog class constructor");
  }

  Dog.myNamedConstructor(String breed, String color) : super.myAnimalNamedConstructor(){
    this.breed = breed;
    print("Dog class Named constructor");
  }
}