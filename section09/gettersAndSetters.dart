
/*
    Objectives
    1. Default getter and setter
    2. Custom getter and setter
    2. private instance variable
 */
void main(){
    var student = Student(); // callind default setter to set value
    student.name ="Peter";  // Calling default getter to for value
    student.percentage = 435;
    print(student.name);
    print(student.percentage);
}

class Student {
    String name;    // Instabe variable with default getter and setter

    double _percent;  // private instance variable fot its own library


    void set percentage(double marksSecured) => _percent = (marksSecured / 500) * 100;

        double get percentage => _percent;

}