void main(){

  /*
    Fail
   var circle = Circle();
   circle.pi
   */

   print(Circle.pi);
   Circle.calculateArea();

}

class Circle{
  static double pi = 3.1415;
  static int maxRadius = 5;
  String color;

  static void calculateArea() {
    print("Something circle stuff");
    // myNormalFunction();  Not allowed to call instance function
    // this.color; you cannot use 'this' keyword and even cannot access Instance
    //    variable.
  }

  void myNormalFunction() {
    Circle.calculateArea();
    this.color="Red";
    print(Circle.pi);
    print(Circle.maxRadius);
  }
}