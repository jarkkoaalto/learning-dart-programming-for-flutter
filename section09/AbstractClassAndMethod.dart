/*
  Objectives
  1. Abstract method
  2. Abstract Class

 */

void main() {
 //  var shape = Shspe(); // Error. Cannot instantiate Abstract class

  var rectangle = Rectangle();
  rectangle.draw();

  var circle = Circle();
  circle.draw();
}

abstract class Shape{

  // Define your Instance variable if needed
  int x;
  int y;
  void draw(); // Abstract method
  void myNormalFunction(){
    // Code here...
  }
}

class Rectangle extends Shape{
  void draw(){
    // Code here ...
    print("Drawing Rectangle ...");
  }
}

class Circle extends Shape{
  void draw(){
    print("Drawing Circle...");
  }
}