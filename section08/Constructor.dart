
/*
  Objectives
  1. Default constructor
  2. Parameterized constructors
  3. Named Constructors
  4. Constant Constructors
 */

void main(){
  var student1 = Student(23,"Perer"); // One object, student2 id reference variable
  print("${student1.name} and ${student1.id}");
  student1.study();
  student1.sleep();


  var student2  =Student(45,"Sam"); // One object, student2 id reference variable
  print("${student2.name} and ${student2.id}");
  student2.study();
  student2.sleep();

  var student3 = Student.myCustomConstructor(); // one object student3 ...
  student3.id = 43;
  student3.name ="Gynter";
  print("${student3.name} and ${student3.id}");
  student3.study();
  student3.sleep();

  var student4 = Student.myAnotherCustomConstructor(4,"Paul"); //one object student4 ...
  print("${student4.name} and ${student4.id}");
  student4.study();
  student4.sleep();
}

// Define states and behavior of a student
class Student {
  int id = -1;  // instace of field variable, default value is null
  String name; // instace of field variable, default value is numm


  Student(this.id, this.name); // Parameterised Constructor

  Student.myCustomConstructor(){
    // Your Code
    print("Thisis my constom constructor"); // Named Constructor
  }

  Student.myAnotherCustomConstructor(this.id, this.name); // Named Constructor

  void study() {
    print("${this.name} is now studying");

  }

  void sleep() {
    print("${this.name} is now sleeping");

  }
}