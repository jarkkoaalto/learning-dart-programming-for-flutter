void main(){
  var student1 = Student();
  student1.id = 23;
  student1.name = "Peter";

  print("${student1.name} and ${student1.id}");
  student1.study();
  student1.sleep();


  var student2  =Student();
  student2.id = 24;
  student2.name = "Sammy";

  print("${student2.name} and ${student2.id}");
  student2.study();
  student2.sleep();
}

// Define states and behavior of a student

class Student {
      int id = -1;  // instace of field variable, default value is null
      String name; // instace of field variable, default value is numm

      void study() {
        print("${this.name} is now studying");

    }

      void sleep() {
        print("${this.name} is now sleeping");

    }
}