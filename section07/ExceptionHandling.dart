/*

  Objectives
  1. On clause
  2. Catch clause with exception object
  3. Catch clause with exception object and stacktrace objects
  4. Finally
  5. create our own custom excetion
 */

void main(){

    print("CASE 1");
    try {
      int result = 12 ~/ 2;
      print("The result is $result");
    }on IntegerDivisionByZeroException{
      print("Cannot divide by Zero");
    }


    print("");print("CASE 2");
    // Case 2: When you do not know the execption use Catch Clause
    try{
      int result = 12 ~/ 2;
      print("The result is $result");
    }catch (e){
      print("Cannot divide by Zero");
    }

    print("");print("CASE 3");
    // Case 2: Using steack trace to know the events occurred before was thrown
    try{
      int result = 12 ~/ 2;
      print("The result is $result");
    }catch (e,s){
      print("The exception throws is $e");
      print("Stack Trace \n $s");
    }

    print("");print("CASE 4");
    // Case 2: When you do not know the execption use Catch Clause
    try{
      int result = 12 ~/ 2;
      print("The result is $result");
    }catch (e){
      print("The exception throws is $e");
    }finally{
      print("This is Finally Clauce. This clause always executed");
    }


    print("");print("CASE 5");
    // Case 5: custom execption
    try {
      depositMoney(-200);
    }catch (e){
      print(e.errorMessage());
    }
}
class DepositException implements Exception{
    String errorMessage(){
        return "You cannot enter amount less than 0";
  }
}
void depositMoney(int amount){
    if(amount < 0){
      throw new DepositException();
  }
}

