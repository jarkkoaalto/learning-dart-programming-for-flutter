void main() {

  /*
     Condirional expressions

     1. condition ? exp1 : exp2
      If condition is true, ecaluates expr1 ( and returns its value)
      otherwise, evaluates and returns the value of expr2
   */

      int a = 2;
      int b = 3;
      int smallNumber;


      if(a<b){
      smallNumber = a;//   print("$a is smaller")
      }else{
        smallNumber = b;// print("$b is smaller")
      }

      print("$smallNumber is smallers");

      // a < b ? print("$a is smaller") : print("$b is smaller");
      smallNumber = a < b ? a : b;
      print("$smallNumber is small number" );


      /*
       2. expr1 ?? expr2
       If expr1 is non-null, returns its value; othervise, evalueats and
       returns the value of expr2
       */
        String name  = null;
        String nameToPrint = name ?? "Guest User";
        print(nameToPrint);


}
