void main() {

    /*
      Switch case statement: Applicable for only 'int' and 'String'
     */
      String grade = 'F';
      switch (grade){
        case 'A':
          print("Exellent crade of A");
          break;
        case 'B':
          print("very good!!");
          break;
        case 'C':
          print("Good enough, but work hard");
          break;
        case 'F':
          print("Fail");
          break;
        default:
          print("Invalid input");
      }

}