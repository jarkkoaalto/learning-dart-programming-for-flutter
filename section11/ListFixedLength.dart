void main(){
  // Elements  N N N N N N
  // index:    0 1 2 3 4 5
  List<int> numbersList = List(6); // Fixed length list
  numbersList[0] = 13;
  numbersList[1] = 99;
  numbersList[2] = 1;
  numbersList[3] = 2;
  numbersList[4] = 5;
  numbersList[5] = 52;
  print(numbersList[3]);
  print("");

  for(int ele in numbersList){
    print(ele);
  }

  print("");
  numbersList.forEach((ele) => print(ele));


  print("");
  for (int i =0;i<numbersList.length; i++){
    print(numbersList[i]);
  }
}