

void main(){

  Map<String, String> fruits = Map();
  fruits["apple"] = "red";
  fruits["banana"] = "yellow";
  fruits["guava"] = "green";

  print(fruits["apple"]);
  print("");

  for(String key in fruits.keys){
    print(key);
  }

  print("");

  for(String values in fruits.values){
    print(values);
  }
  print("");
  fruits.forEach((key,value) => print("key: $key value: $value"));

}