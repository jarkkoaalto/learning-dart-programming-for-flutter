
void main(){

  /*
  * Difference between final and const
  * final variable can only be set once and it is initialize
  *  when access.
  */

  // Final ketword
  final name = "Peter";
  final cityName = "Mumbai";
  final String country = "India";

  // Consta keyword
  const PI = 3.1415;
  const double gravity = 9.82;

  Circle s = new Circle();
  print(s.color);
}

class Circle{
    final color = "Red";
    static const PI = 3.14;
}